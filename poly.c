/*
 * Carl Angelo Araya
 * 201508809
 * THXY
 *
 * poly.c: Rational polynomial roots calculator
 *
 * First, this program asks for the degree of the polynomial the user is
 * about to enter. Then it asks for all the coefficients, sorted in increasing
 * order from the constant to the leading coefficient. After that, it prints
 * all the roots, sorted in increasing order (if there are any). Finally, the
 * program asks if the user wants to continue.
 */

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 8

/*
 * Get degree from user and return it (NOTE: this subprogram has flawless input
 * validation and protection against buffer overflow attacks)
 */
int get_degree(void);

/*
 * Get coefficient input from user (just like the above, this subprogram has
 * flawless input validation and protection against buffer overflow attacks).
 */
void get_coeff(int n, int coeffStorage[]);

/*
 * Compute all the rational roots of input polynomial and store the output
 * to pRoots and qRoots array. pRoots and qRoots are parallel arrays containing
 * the numerators and denominators of the roots, respectively. Return the
 * number of rational roots.
 */
int get_r_roots(int n, int coeff[], int pRoots[], int qRoots[]);

/*
 * Ask if the user wants to type a new polynomial then return a boolean value
 */
int ask_new(void);

/*
 * Return greates common diviser of 2 numbers.
 */
int gcd(int a, int b);

/*
 * Return integer absolute value of number.
 */
int iAbs(int n);

/*
 * Computes floor(sqrt(n)).
 */
int iSqrt(int n);

/*
 * Insert new fraction into the sorted parallel array, properly.
 *
 */
int insert_frac(int p, int q, int pArray[], int qArray[], int n);

/*
 * Compare 2 fractions. Output is similar to strcmp.
 */
int frac_cmp(int p1, int q1, int p2, int q2);


int main(void)
{
    int isNew = 1;
    do
    {
        // Degree of polynomial
        int n;
        n = get_degree();

        // Coefficients
        int coeff[n+1];
        get_coeff(n, coeff);

        // Roots of the polynomial. Numerators will be stored in pRoots and
        // denominators will be stored in qRoots.
        int pRoots[n];
        int qRoots[n];
        int numRoots;
        numRoots = get_r_roots(n, coeff, pRoots, qRoots);

        // Print all roots in a neat and orderly way (no pun intended)
        if (numRoots == 0)
            printf("The input has no rational roots.\n");
        else
        {
            printf("The rational roots of the input polynomial are:\n");

            int i;
            for (i = 0; i < numRoots - 1; i++)
            {
                printf("%d", pRoots[i]);
                if (qRoots[i] != 1)
                    printf("/%d", qRoots[i]);
                printf(", ");
            }

            printf("%d", pRoots[numRoots - 1]);
            if (qRoots[numRoots - 1] != 1)
                printf("/%d", qRoots[numRoots - 1]);
            printf("\n");
        }

        // Ask for user if he wants to enter a new polynomial
        isNew = ask_new();
        printf("\n");
    } while (isNew);

    printf("Terminating program.\n");
    return 0;
}

int get_degree(void)
{
    int n;
    char buffer[BUFSIZE];
    char ch;
    int inputvalid = 0;

    // Get degree from user (int variable n) and check for invalid inputs
    do
    {
        printf("Enter the highest degree of the input polynomial: ");
        int foundbadchar = 0;
        int i;
        // Checks each character meticulously
        for (i = 0; i < BUFSIZE - 1; i++)
        {
            ch = getchar();
            if (ch == '\n')
                break;
            else if (ch == ' ' || ch == '\t')
                i--;
            else if (!isdigit(ch))
            {
                foundbadchar = 1;
                break;
            }
            else 
                buffer[i] = ch;
        }
        buffer[i] = '\0';

        // If input too large or bad char is found
        if (i == BUFSIZE - 1 || foundbadchar)
        {
            while (ch != '\n')
                ch = getchar();
            printf("ERROR: Invalid input.\n");
        }
        else
        {
            n = atoi(buffer);
            if (n < 0 || (n == 0 && buffer[0] != '0'))
                printf("ERROR: Invalid input.\n");
            else
                inputvalid = 1;
        }
    } while (!inputvalid);
    return n;
}

void get_coeff(int n, int coeffStorage[])
{
    char ch;
    int inputvalid;
    do
    {
        inputvalid = 1;
        printf("Enter %d integer coefficients ", n+1);
        printf("starting from the 0th degree.\n");
        printf("Separate each input by a comma: ");
        int i;
        for (i = 0; i <= n; i++)
        {
            // "Peeks" to next char in the input stream
            char nextch = getchar();
            ungetc(nextch, stdin);
            if (nextch == '\n')
            {
                printf("ERROR: Invalid input.\n");
                getchar();
                inputvalid = 0;
                break;
            }
            
            // Gets user input for each integer, then checks for ALL bad cases
            int success = scanf("%d", &coeffStorage[i]);

            // Skip some whitespace
            do
            {
                scanf("%c", &ch);
            } while (ch == ' ' || ch == '\t');

            if (success != 1
                    || (i != n && ch != ',')
                    || (i == n && ch != '\n')
                    || (ch == ' '))
            {
                printf("ERROR: Invalid input.\n");
                while (ch != '\n')
                {
                    ch = getchar();
                }
                inputvalid = 0;
                break;
            }
        }
        // If leading coefficient is 0
        if (coeffStorage[n] == 0 && inputvalid)
        {
            printf("ERROR: Invalid input.\n");
            inputvalid = 0;
        }
    } while (!inputvalid);
}

/*
 * This is where the magic happens.
 * This function uses synthetic division. Checks if a remainder of 0 is found.
 * Also checks for non-integers in the synthetic division process.
 * Return the number of roots found.
 */
int get_r_roots(int n, int coeff[], int pRoots[], int qRoots[])
{
    int maxRoots = n;
    int numRoots = 0;

    // Take care of polynomials without a unit coefficient
    while (coeff[0] == 0)
    {
        int i;
        for (i = 0; i < n; i++)
            coeff[i] = coeff[i+1];
        if (insert_frac(0, 1, pRoots, qRoots, numRoots) == 1)
            numRoots++;
        n--;
    }

    if (n == 0) return numRoots; /* special case */

    // Get the maximum values of p and q. They will be in lowest terms.
    int maxp = iAbs(coeff[0]);
    int maxq = iAbs(coeff[n]);

    /*
     * We only need to check until the square root of maxp.
     * Each factor that is found in the loop will have its partner, which is
     * also a factor.
     */
    int sqmaxp = iSqrt(maxp);
    int sqmaxq = iSqrt(maxq);

    /*
     * This loop will have these values for px: 1,-1,2,-2,3,-3,4,-4...
     * (sorry about the confusing ternary operator in there)
     */
    int px;
    for (px = 1; px <= sqmaxp; px -= (px < 0) ? 1 : 0, px *= -1)
    {
        // Check if px is a factor of maxp
        if (maxp % px == 0)
        {
            // This loop will have these values for qx: 1,2,3,4,..
            int qx;
            for (qx = 1; qx <= sqmaxq; qx++)
            {
                // Check if qx is a factor of maxp
                if (maxq % qx == 0)
                {
                    /*
                     * {px, maxp / px} is what's called a factor pair.
                     * An example is 36 which has factor pairs:
                     *   {1,36},{2,18},{3,12},{4,9},{6,6}.
                     * The for loop under this will go through each
                     * member of the factor pair.
                     */
                    int pairp[] = {px, maxp / px};

                    int uniquep;
                    if (pairp[0] == pairp[1])
                        uniquep = 1;
                    else
                        uniquep = 2;

                    int pi;
                    for (pi = 0; pi < uniquep; pi++)
                    {
                        /*
                         * Similarly, {qx, maxq / qx} is a factor pair.
                         * Only this time, it's for q.
                         */
                        int pairq[] = {qx, maxq / qx};

                        int uniqueq;
                        if (pairq[0] == pairq[1])
                            uniqueq = 1;
                        else
                            uniqueq = 2;

                        int qi;
                        for (qi = 0; qi < uniqueq; qi++)
                        {
                            /*
                             * p/q is a possible root, and this part of the
                             * program will test if it is a legitimate one.
                             * NOTE: only integer data types are used here,
                             * for the sake of accuracy. In this case, long
                             * long might be necessary.
                             */
                            long long p = pairp[pi];
                            long long q = pairq[qi];

                            long long gcdpq = gcd(iAbs(p), q);
                            p /= gcdpq;
                            q /= gcdpq;

                            int isRoot = 1;
                            long long temp = 0;
                            int i;
                            /*
                             * Go through the process of synthetic division.
                             * temp is a placeholder variable.
                             */
                            for (i = n; i >= 0; i--)
                            {
                                temp *= p;
                                temp += coeff[i];
                                /*
                                 * This condition is what I meant by checking
                                 * for non-integers in the synthetic division
                                 * process. Since we will divide temp by q, we
                                 * first need to check if temp is divisible by
                                 * q. If not then there's no way it can ever
                                 * be a root.
                                 */
                                if (temp % q != 0)
                                {
                                    isRoot = 0;
                                    break;
                                }
                                temp /= q;
                            }

                            // If remainder is 0 it means p/q is a true root
                            if (temp == 0 && isRoot)
                            {
                                int addsuccess;
                                addsuccess = insert_frac(p, q,
                                        pRoots, qRoots, numRoots);
                                if (addsuccess)
                                    numRoots++;

                                if (numRoots == maxRoots)
                                    return numRoots;
                            }
                        }
                    }
                }                    /* sooo many closing braces */
            }
        }
    }
    return numRoots;
}

int ask_new(void)
{
    char buffer[4];
    while (1)
    {
        printf("Input new polynomial? ");

        int isemptystdin = 0;

        // Gets user input
        int i;
        for (i = 0; i < 4; i++)
        {
            buffer[i] = tolower(getchar());
            if (buffer[i] == ' ' || buffer[i] == '\t')
                i--;
            else if (buffer[i] == '\n')
            {
                isemptystdin = 1;
                break;
            }
        }
        buffer[i] = '\0';

        // Checks for legitimate input
        if (strcmp(buffer, "yes") == 0
                || strcmp(buffer, "y") == 0)
        {
            return 1;
        }
        else if (strcmp(buffer, "no") == 0
                || strcmp(buffer, "n") == 0)
        {
            return 0;
        }
        else
        {
            printf("ERROR: Invalid input.\n");
            if (!isemptystdin)
                while (buffer[i] != '\n')
                    buffer[i] = getchar();
        }
    }
}

int gcd(int a, int b)
{
    while (a != 0 && b != 0)
    {
        if (a > b) a %= b;
        else b %= a;
    }
    if (a == 0) return b;
    else return a;
}

int iAbs(int n)
{
    return n < 0 ? -1 * n : n;
}

/*
 * Computes floor(sqrt(n)).
 * Uses an integer variant of Newton's method.
 */
int iSqrt(int n)
{
    int x0;
    int x1 = n;
    int x2 = n;
    do
    {
        x0 = x1;
        x1 = x2;
        x2 = (x1 + n/x1)/2;
    } while (x1 != x2 && x0 != x2);
    return x1;
}

int insert_frac(int p, int q, int pArray[], int qArray[], int n)
{
    int i;
    for (i = 0; i < n && frac_cmp(p, q, pArray[i], qArray[i]) > 0; i++);
    if (n != 0 && p == pArray[i] && q == qArray[i])
    {
        return 0;
    }
    int j;
    for (j = n; j > i; j--)
    {
        pArray[j] = pArray[j-1];
        qArray[j] = qArray[j-1];
    }
    pArray[i] = p;
    qArray[i] = q;

    return 1;
}

int frac_cmp(int p1, int q1, int p2, int q2)
{
    long long m1 = p1 * q2;
    long long m2 = p2 * q1;
    if (m1 > m2)
    {
        return 1;
    }
    else if (m1 < m2)
    {
        return -1;
    }
    else
    {
        return 0;
    }
}
